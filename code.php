<?php

class Building {

	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name,$floors,$address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}
    public function getName(){
		return $this->name;
	}
    public function setName($name){
		if(gettype($name) === "string"){
			$this->name = $name;
		}
	}
    public function getFloor(){
		return $this->floors;
	}
    public function getAddress(){
		return $this->address;
	}
	
}


class Condominium extends Building{

	public function getName(){
		return $this->name;
	}
    public function setName($name){
		if(gettype($name) === "string"){
			$this->name = $name;
		}
	}
	public function getFloor(){
		return $this->floors;
	}
    public function setFloor($floors){
		$this->floors = $floors;
	}
    public function getAddress(){
		return $this->address;
	}
    public function setAddress($address){
		$this->address = $address;
	}
}
$building = new Building('Caswynn Building',8,'Timog Avenue, Quezon City, Philippines');
$condominium = new Condominium('Enzo Condo',5,'Buendia Avenue, Makati City, Philippines');
